FROM node:18
WORKDIR /usr/src/app
COPY ./graph-app/package*.json ./
RUN npm install
COPY ./graph-app/index.js .
EXPOSE 8080
CMD [ "node", "index.js" ]