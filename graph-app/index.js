const client = require('prom-client');
const express = require('express');

const app = express();
// create a registery and pull metrics
const register = new client.Registry();
client.collectDefaultMetrics({register});

const hostname = '0.0.0.0';
const port = 8080;

app.get('', async(req, res) => {
  res.statusCode = 200;
  res.setHeader('Content-Type', 'text/plain');
  res.end('Welcome to my graph app.')
});

app.get('/metrics', async(req, res) => {
    res.setHeader('Content-Type', register.contentType);
    res.send(await register.metrics());
});

app.listen(port, () => {
  console.log(`Server running on http://localhost:8080, metrics are exposed on http://localhost:8080/metrics`);
});